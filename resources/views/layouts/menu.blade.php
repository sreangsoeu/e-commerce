<li class="menu-title">Menu</li>
<li>
    <a href="index.html" class="waves-effect">
        <i class="ri-home-gear-line"></i>
        <span>{{__('menu.home')}}</span>
    </a>
</li>

<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="ri-layout-3-line"></i>
        <span>Layouts</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li>
            <a href="javascript: void(0);" class="has-arrow">Vertical</a>
            <ul class="sub-menu" aria-expanded="true">
                <li><a href="layouts-dark-sidebar.html">Dark Sidebar</a></li>
                <li><a href="layouts-compact-sidebar.html">Compact Sidebar</a></li>
                <li><a href="layouts-icon-sidebar.html">Icon Sidebar</a></li>
                <li><a href="layouts-boxed.html">Boxed Layout</a></li>
                <li><a href="layouts-preloader.html">Preloader</a></li>
                <li><a href="layouts-colored-sidebar.html">Colored Sidebar</a></li>
            </ul>
        </li>

        <li>
            <a href="javascript: void(0);" class="has-arrow">Horizontal</a>
            <ul class="sub-menu" aria-expanded="true">
                <li><a href="layouts-horizontal.html">Horizontal</a></li>
                <li><a href="layouts-hori-topbar-light.html">Topbar light</a></li>
                <li><a href="layouts-hori-boxed-width.html">Boxed width</a></li>
                <li><a href="layouts-hori-preloader.html">Preloader</a></li>
                <li><a href="layouts-hori-colored-header.html">Colored Header</a></li>
            </ul>
        </li>
    </ul>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="ri-account-circle-line"></i>
        <span>Authentication</span>
    </a>
    <ul class="sub-menu" aria-expanded="false">
        <li><a href="auth-login.html">Login</a></li>
    </ul>
</li>
{{-- <li>
    <a href="{{ route('brand.index') }}" class=" waves-effect">
        <i class="ri-account-circle-line"></i>
        <span>Brand</span>
    </a>
</li> --}}
<li>
    <a href="{{ route('users.index') }}" class=" waves-effect">
        <i class="ri-account-circle-line"></i>
        <span>{{__('menu.users')}}</span>
    </a>
</li>
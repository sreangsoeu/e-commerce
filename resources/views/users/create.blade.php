<div class="modal-header bg-info">
    <h6 class="modal-title m-0 text-white" id="myExtraLargeModalLabel">User Create</h6>
    <a href="javascript:void(0)" class="text-white" data-bs-dismiss="modal"><i class="fa fa-times-circle text-white"
            aria-hidden="true"></i></a>
</div>
<!--end modal-header-->
{!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
    <div class="modal-body">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.first_name_kh')</label>
                        {!! Form::text('name', null, array('placeholder' => __('app.first_name_kh'),'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.last_name_kh')</label>
                        {!! Form::text('name', null, array('placeholder' => __('app.last_name_kh'),'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.first_name_en')</label>
                        {!! Form::text('name', null, array('placeholder' => __('app.first_name_kh'),'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.first_name_en')</label>
                        {!! Form::text('name', null, array('placeholder' => __('app.first_name_en'),'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.email')</label>
                        {!! Form::text('name', null, array('placeholder' => __('app.email'),'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.select') @lang('app.role')</label>
                        {!! Form::select('name',['1'=>'A','2'=>'B'], null, array('placeholder' => __('app.select').' '.__('app.role'),'class' => 'form-control select2')) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.email')</label>
                        {!! Form::text('name', null, array('placeholder' => __('app.email'),'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="mb-3">
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">@lang('app.retype_password')</label>
                        {!! Form::text('name', null, array('placeholder' => __('app.retype_password'),'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="mb-3">
                    <input name="file1" type="file" class="dropify" data-height="100" />
                </div>
            </div>
        </div>
    </div>
    <!--end modal-body-->
    <div class="modal-footer">
        <button type="button" class="btn btn-info border-0">Save changes</button>
        <button type="button" class="btn btn-soft-secondary border-0" data-bs-dismiss="modal">Close</button>
    </div>

{!! Form::close() !!}
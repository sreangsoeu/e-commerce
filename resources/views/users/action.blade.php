<td>
    <a href="javascript:void(0);" class="me-3" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Edit" aria-label="Edit"><i class="mdi mdi-pencil font-size-18 text-info"></i></a>
    <a href="javascript:void(0);" onclick="userDelete({{ $user['id'] }})" class="" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Delete" aria-label="Delete"><i class="mdi mdi-trash-can font-size-18 text-danger"></i></a>
</td>
@extends('layouts.master')
@section('content')
@section('title','User List')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">/ User List</li>
                    </ol>
                </div>
                    <button type="button" onclick="createUser()" class="btn btn-info text-white waves-effect waves-light btn-sm border-0">
                        <i class="fas fa-plus-circle text-white"></i> Add New
                    </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-xl" data-bs-backdrop="static" data-bs-keyboard="false" id="bd-example-modal-xl" tabindex="-1" role="dialog"
        aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
               
                <!--end modal-footer-->
            </div>
            <!--end modal-content-->
        </div>
        <!--end modal-dialog-->
    </div>
    <!--end modal-->
@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
    <script>
        function createUser(){
            $.ajax({
                type: "GET",
                url: "{{ route('users.create') }}",
                dataType: "html",
                success: function (res) {
                    $(".modal-content").html(res);
                    $('.select2').select2({
                        dropdownParent: $('.modal')
                    });
                    $('.dropify').dropify();
                    $("#bd-example-modal-xl").modal('show');
                }
            });
        }
    </script>
@endpush
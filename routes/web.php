<?php

use App\Http\Middleware\GetLocale;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Changelanguage;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\APIController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BrandsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/dasboard', function () {
    return view('layouts.master');
});
Auth::routes();
Route::get('/set-language/{lang}', [Changelanguage::class, 'setLocale'])->name('set-language');


Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::get('users-delete', [UserController::class, 'userDelete'])->name('users.userDelete');
    Route::resource('products', ProductController::class);
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/brand', [BrandsController::class, 'index'])->name('brand.index');

//Show Form Create User
// Route::get('users-form', [UserController::class, 'userCreate'])->name('users.create_form');

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/epm', [APIController::class, 'epm'])->name('api-epm');
Route::post('/my/store', [APIController::class, 'store'])->name('api-store');
Route::get('/my/update', [APIController::class, 'update'])->name('api-store');
Route::get('/my/delete', [APIController::class, 'delete'])->name('api-delete');
<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addIndexColumn()
            ->addColumn('role_name', function ($user) {
                $role_name = DB::table('model_has_roles')->where('model_has_roles.model_id', $user->id)
                ->join('roles', 'roles.id', 'model_has_roles.role_id')->first()->name??'N/A';
                return $role_name;
            })
            ->addColumn('date', function ($user) {
                $created_at = date("d-F-Y", strtotime($user->created_at??'1970-01-01'));
                return $created_at;
            })
            ->addColumn('action', function ($user) {
                return view('users.action',compact('user'));
            })
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model): QueryBuilder
    {
        $data = $model;
        return $data->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('datatable')
                    ->addTableClass('table table-bordered  nowrap w-100')
                    ->columns($this->getColumns())
                    ->minifiedAjax();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::computed('DT_RowIndex',__('users.no'))->addClass('text-center'),
            Column::make('first_name')->title(__('users.first_name'))->addClass('text-center')->searchable('first_name'),
            Column::make('last_name')->title(__('users.last_name'))->addClass('text-center'),
            Column::make('email')->title(__('users.email'))->addClass('text-center'),
            Column::make('date')->title(__('users.date'))->addClass('text-center'),
            Column::make('role_name')->title(__('users.role'))->addClass('text-center'),
            Column::computed('action')->title(__('users.action'))->addClass('text-center'),
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Users_' . date('YmdHis');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    use HasFactory;

    public function getCategory()
    {
        return $this->hasMany(Categories::class,'brand_id');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class APIController extends Controller
{
    public function index(){
        $user = Http::get("http://localhost/sreang/school/api/epm");
        return view('testapi.index',compact('user'));
    }

    public function epm(Request $request)
    {
        $user = DB::table('users')->get();
        $emp = ['data'=> $user];
        return response()->json($emp);
    }

    public function store(Request $request)
    {
        $posts = User::create([
            'name'=> $request->name,
            'email'=> $request->email,
            'password' => Hash::make($request->password),
        ]);
        $arrayName = array('status' => 200, 'data'=>$posts);
        return $arrayName;
    }

    public function update(Request $request)
    {
        $posts = User::where('id',$request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $arrayName = array('status' => 200, 'data' => $posts);
        return $arrayName;
    }

    public function delete(Request $request)
    {
        $posts = User::where('id', $request->id)->delete();
        $arrayName = array('status' => 200, 'data' => $posts);
        return $arrayName;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    public function index(){
        // $brand = Brands::join('categories', 'categories.brand_id', 'brands.id')
        // ->select(
        //     'brands.id as brand_id',
        //     'brands.name as brand_name',
        //     'categories.id as cate_id',
        //     'categories.name as cate_name',
        // )
        // ->get();

        $brand = Brands::get();
        // dd($brand);
        return view('brand.index', compact('brand'));
    }
}

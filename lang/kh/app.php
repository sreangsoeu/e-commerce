<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'first_name_en' => 'នាមត្រកូល (អង្គគ្លេស)',
    'last_name_en' => 'នាមខ្លួន (អង្គគ្លេស)',
    'first_name_kh' => 'នាមត្រកូល (ខ្មែរ)',
    'last_name_kh' => 'នាមខ្លួន (ខ្មែរ)',
    'email' => 'អុីម៉ែល',
    'password' => 'លេខសម្ងាត់',
    'retype_password' => 'វាយ​លេខសម្ងាត់​ម្តង​ទៀត',
    'role' => 'សិទ្ធ',
    'select' => 'ជ្រើសរើស',
    'action' => 'ជម្រើស',
];

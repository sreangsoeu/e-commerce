<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'users_list' => 'តារាង អ្នកប្រើប្រាស់',
    'users_create' => 'បង្កើត អ្នកប្រើប្រាស់',
    'users_add' => 'បន្ថែម អ្នកប្រើប្រាស់',
    'users_edit' => 'កែប្រែ អ្នកប្រើប្រាស់',
    'users_detail' => 'បញ្ជាក់',

    'no' => 'ល.រ',
    'first_name' => 'នាមត្រកូល',
    'last_name' => 'នាមខ្លួន',
    'email' => 'អុីម៉ែល',
    'role' => 'សិទ្ធ',
    'date' => 'កាលបរិច្ឆេទ',
    'action' => 'ជម្រើស',
];

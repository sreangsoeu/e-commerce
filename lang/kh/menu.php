<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'ទំព័រដើម',
    'users' => 'អ្នកប្រីប្រាស់',
    'list' => 'តារាង',
    'user_list'=>'តារាងអ្នកប្រើប្រាស់',
    'language' => 'kh',

];

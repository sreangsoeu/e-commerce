<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'no' => 'No',
    'users_list' => 'Users List',
    'users_create' => 'Users Create',
    'users_add' => 'Users Add',
    'users_edit' => 'Users Edit',
    'users_detail' => 'Users Detail',

    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'Email',
    'role' => 'Role',
    'date' => 'Date',
    'action' => 'Action',
    
];

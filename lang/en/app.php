<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'first_name_kh' => 'First Name (Kh)',
    'last_name_kh' => 'Last Name (Kh)',
    'first_name_en' => 'First Name (En)',
    'last_name_en' => 'Last Name (En)',
    'email' => 'Email',
    'password' => 'Password',
    'retype_password' => 'Retype Password',
    'role' => 'Role',
    'select' => 'Select',
    'action' => 'Action',
];
